package ru.lapin.image_viewer;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import ru.lapin.image_viewer.config.Configuration;
import ru.lapin.image_viewer.config.Utils;
import ru.lapin.image_viewer.decoder.AnimationImage;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.function.Consumer;

import static java.awt.GridBagConstraints.BOTH;
import static java.awt.GridBagConstraints.CENTER;
import static org.apache.commons.lang3.math.NumberUtils.min;
import static ru.lapin.image_viewer.config.ImageSource.IS;
import static ru.lapin.image_viewer.config.Utils.classToConfig;

public class ImagePanel extends JPanel {
    private static final String CLASS_KEY = classToConfig(ImagePanel.class);

    private static final Configuration.Element<Double> MAX_SIZE_PIXEL =
        new Configuration.Element<>(CLASS_KEY + "max_size_pixel", Double::parseDouble, null, 20.);
    private static final Configuration.Element<Double> STEP_SCALING_MOUSE_WHEEL =
        new Configuration.Element<>(CLASS_KEY + "step_scaling_mouse_wheel", Double::parseDouble, null, 1.5);

    private static final String LABEL_ZOOM_FORMAT = "%s%%";
    private static final int MAX_ANIMATION_SLIDER = 101;
    private static final double K_CENTER = 0.5;
    private final JScrollPane scrollPane = new JScrollPane();

    private final Configuration configuration;
    private final ScalableImagePanel imagePanel;
    private final JScrollBar verticalBar;
    private final JScrollBar horizontalBar;
    private final SliderZoomChangeListener sliderZoomChangeListener;
    private final JSlider zoomSlider;
    private final JLabel zoomLabel;

    public ImagePanel(Configuration configuration, AnimationImage image, Runnable openDirectory) {
        super(new GridBagLayout());

        this.configuration = configuration;

        add(scrollPane, gridBagConstraints(0, 0, 1, 1));

        imagePanel = new ScalableImagePanel(configuration, image);
        scrollPane.setViewportView(imagePanel);

        ImageMouseListener mouseListener = new ImageMouseListener();
        scrollPane.addMouseListener(mouseListener);
        scrollPane.addMouseMotionListener(mouseListener);
        scrollPane.addMouseWheelListener(mouseListener);

        scrollPane.setWheelScrollingEnabled(false);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.addComponentListener(new ScrollRootPanelComponentListener());

        verticalBar = scrollPane.getVerticalScrollBar();
        horizontalBar = scrollPane.getHorizontalScrollBar();


        JPanel toolsPanel = new JPanel(new GridBagLayout());
        add(toolsPanel, gridBagConstraints(0, 1, 1, 0));
        int x = 0;
        toolsPanel.add(borderPanel(), gridBagConstraints(x++, 0, 0, 0));
        toolsPanel.add(createButton(IS.getOpenDirectory(), "open directory", openDirectory),
            gridBagConstraints(x++, 0, 0, 0)
        );

        if (imagePanel.isAnimated()) {
            toolsPanel.add(borderPanel(), gridBagConstraints(x++, 0, 0, 0));
            JButton startButton = createButton(IS.getPlay(), "start", imagePanel::start);
            toolsPanel.add(startButton, gridBagConstraints(x++, 0, 0, 0));

            JButton stopButton = createButton(IS.getStop(), "stop", imagePanel::stop);
            toolsPanel.add(stopButton, gridBagConstraints(x++, 0, 0, 0));

            imagePanel.addRunListener(v -> {
                startButton.setVisible(!v);
                stopButton.setVisible(v);
            });

            JSlider sliderAnimation = new JSlider(0, MAX_ANIMATION_SLIDER - 1);
            SliderAnimationChangeListener animationListener = new SliderAnimationChangeListener(sliderAnimation);
            sliderAnimation.addChangeListener(animationListener);
            imagePanel.addTimeListener(animationListener);
            toolsPanel.add(sliderAnimation, gridBagConstraints(x++, 0, 0, 0));

            JLabel labelIndex = new JLabel();
            labelIndex.setPreferredSize(new Dimension(30, 0));
            labelIndex.setHorizontalAlignment(SwingConstants.RIGHT);
            imagePanel.addIndexListener(i -> labelIndex.setText(String.valueOf(i + 1)));
            toolsPanel.add(labelIndex, gridBagConstraints(x++, 0, 0, 0));

            toolsPanel.add(new JLabel("/" + imagePanel.countFrames()), gridBagConstraints(x++, 0, 0, 0));
        }
        toolsPanel.add(borderPanel(), gridBagConstraints(x++, 0, 1, 0));

        zoomLabel = new JLabel();
        zoomLabel.setPreferredSize(new Dimension(35, 0));
        toolsPanel.add(zoomLabel, gridBagConstraints(x++, 0, 0, 0));

        zoomSlider = new JSlider();
        sliderZoomChangeListener = new SliderZoomChangeListener();
        zoomSlider.addChangeListener(sliderZoomChangeListener);
        zoomSlider.setValue(0);
        toolsPanel.add(zoomSlider, gridBagConstraints(x++, 0, 0, 0));

        toolsPanel.add(createButton(IS.getOneToOne(), "real size", this::setRealSize),
            gridBagConstraints(x++, 0, 0, 0)
        );

        toolsPanel.add(borderPanel(), gridBagConstraints(x, 0, 0, 0));

        imagePanel.start();
    }

    public void stop() {
        imagePanel.stop();
    }

    private void setRealSize() {
        sliderZoomChangeListener.setValue(scaleToValue(1));
        changeScale(1, K_CENTER, K_CENTER);
    }

    private double getMinScale() {
        int width = imagePanel.getImageWidth();
        int height = imagePanel.getImageHeight();
        return min(Math.min((double) scrollPane.getWidth(), width) / width, min((double) scrollPane.getHeight(), height) / height);
    }

    private double valueToScale(double value) {
        return scale(value * value, getMinScale(), configuration.get(MAX_SIZE_PIXEL));
    }

    private double scaleToValue(double scale) {
        double min = getMinScale();
        return Math.sqrt(Math.max(scale - min, 0) / (configuration.get(MAX_SIZE_PIXEL) - min));
    }

    private static int countSkipBar(int skipBegin, int sizeShow, int sizeWindow, double deltaS, double kDelta) {
        return (int) (skipBegin * deltaS + (sizeShow * deltaS - sizeWindow) * kDelta);
    }

    private void setBarXValue(int value) {
        horizontalBar.setValue(Math.min(value, imagePanel.scaleWidth - scrollPane.getWidth()));
    }

    private void setBarYValue(int value) {
        verticalBar.setValue(Math.min(value, imagePanel.scaleHeight - scrollPane.getHeight()));
    }

    private void changeScale(double newS, double kWidth, double kHeight) {
        zoomLabel.setText(String.format(LABEL_ZOOM_FORMAT, (int) (newS * 100)));
        double deltaS = newS / imagePanel.getScale();
        imagePanel.scale(newS);
        setBarXValue(countSkipBar(horizontalBar.getValue(), imagePanel.showWidth, scrollPane.getWidth(), deltaS, kWidth));
        setBarYValue(countSkipBar(verticalBar.getValue(), imagePanel.showHeight, scrollPane.getHeight(), deltaS, kHeight));
    }

    private class ScalableImagePanel extends AnimationImagePanel {
        int showWidth;
        int showHeight;
        int scaleWidth;
        int scaleHeight;
        int shiftX;
        int shiftY;

        public ScalableImagePanel(Configuration configuration, AnimationImage animationImage) {
            super(configuration, animationImage);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g.create(shiftX, shiftY, scaleWidth, scaleHeight));
        }

        @Override
        public Dimension getPreferredSize() {
            int width = scrollPane.getWidth();
            int height = scrollPane.getHeight();
            Dimension superPreferredSize = super.getPreferredSize();
            scaleWidth = superPreferredSize.width;
            scaleHeight = superPreferredSize.height;
            showWidth = Math.min(width, scaleWidth);
            showHeight = Math.min(height, scaleHeight);
            shiftX = (width - showWidth) / 2;
            shiftY = (height - showHeight) / 2;
            return new Dimension(Math.max(width, scaleWidth) * 2, Math.max(height, scaleHeight) * 2);
        }

        @Override
        public void scale(double value) {
            super.scale(value);
            getPreferredSize();
        }
    }

    @RequiredArgsConstructor
    private class SliderAnimationChangeListener implements ChangeListener, Consumer<Double> {
        final JSlider slider;

        boolean ignore = false;

        @Override
        public void stateChanged(ChangeEvent e) {
            if (ignore) return;
            imagePanel.stop();
            imagePanel.setTime((double) slider.getValue() / slider.getMaximum());
        }

        @Override
        public void accept(Double value) {
            ignore = true;
            slider.setValue((int) (value * slider.getMaximum()));
            ignore = false;
        }
    }

    private class ScrollRootPanelComponentListener extends ComponentAdapter {
        int width;
        int height;

        @Override
        public void componentResized(ComponentEvent e) {
            int newWight = scrollPane.getWidth();
            int newHeight = scrollPane.getHeight();
            if (sliderZoomChangeListener.isMin()) changeScale(valueToScale(0), 0, 0);
            else {
                sliderZoomChangeListener.setValue(scaleToValue(imagePanel.getScale()));
                setBarXValue(horizontalBar.getValue() + (int) ((width - newWight) * K_CENTER));
                setBarYValue(verticalBar.getValue() + (int) ((height - newHeight) * K_CENTER));
            }
            width = newWight;
            height = newHeight;
        }
    }

    private class SliderZoomChangeListener implements ChangeListener {
        final int min;
        final double count;
        boolean ignore = false;

        SliderZoomChangeListener() {
            min = zoomSlider.getMinimum();
            count = zoomSlider.getMaximum() - min;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            if (ignore) return;
            changeScale(valueToScale(getValue()), K_CENTER, K_CENTER);
        }

        boolean isMin() {
            return zoomSlider.getValue() == min;
        }

        void setValue(double value) {
            ignore = true;
            zoomSlider.setValue((int) (value * count) + min);
            ignore = false;
        }

        double getValue() {
            return (zoomSlider.getValue() - min) / count;
        }
    }

    private class ImageMouseListener extends MouseAdapter {
        int mouseX;
        int mouseY;
        int scrollBarX;
        int scrollBarY;

        @Override
        public void mousePressed(MouseEvent e) {
            scrollBarX = horizontalBar.getValue();
            scrollBarY = verticalBar.getValue();
            mouseX = e.getX();
            mouseY = e.getY();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            int deltaX = mouseX - e.getX();
            int deltaY = mouseY - e.getY();
            setBarXValue(scrollBarX + deltaX);
            setBarYValue(scrollBarY + deltaY);
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            double scale = imagePanel.getScale();
            double newS = Utils.clamp(
                scale * Math.pow(configuration.get(STEP_SCALING_MOUSE_WHEEL), -e.getWheelRotation()),
                getMinScale(), configuration.get(MAX_SIZE_PIXEL)
            );
            if (newS != scale) {
                sliderZoomChangeListener.setValue(scaleToValue(newS));
                changeScale(newS,
                    Utils.clamp((double) (e.getX() - imagePanel.shiftX) / imagePanel.showWidth, 0, 1),
                    Utils.clamp((double) (e.getY() - imagePanel.shiftY) / imagePanel.showHeight, 0, 1)
                );
            }
        }
    }

    private static JPanel borderPanel() {
        JPanel panel = new JPanel();
        panel.setMinimumSize(new Dimension(10, 0));
        return panel;
    }

    private static GridBagConstraints gridBagConstraints(int gridx, int gridy, double weightx, double weighty) {
        return new GridBagConstraints(gridx, gridy, 1, 1, weightx, weighty, CENTER, BOTH, new Insets(0, 0, 0, 0), 0, 0);
    }

    private static JButton createButton(Icon icon, String title, Runnable runnable) {
        JButton button = new JButton();
        boolean existIcon = icon != null;
        if (existIcon) button.setIcon(icon);
        if (StringUtils.isNotBlank(title)) {
            if (!existIcon) button.setText(title);
            else button.setToolTipText(title);
        }
        if (runnable != null) button.addActionListener(e -> runnable.run());
        return button;
    }

    private static double scale(double value, double min, double max) {
        return ((max - min) * value) + min;
    }
}
