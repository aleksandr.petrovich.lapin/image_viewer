package ru.lapin.image_viewer;

import com.formdev.flatlaf.FlatDarkLaf;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.lapin.image_viewer.config.Configuration;
import ru.lapin.image_viewer.decoder.AnimationImage;
import ru.lapin.image_viewer.decoder.DecoderGif;
import ru.lapin.image_viewer.decoder.DecoderImage;
import ru.lapin.image_viewer.decoder.DecoderList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static ru.lapin.image_viewer.config.Utils.classToConfig;

@Slf4j
public class Main extends JFrame {
    public static final String WORK_DIR =
        new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getFile()).getParent();
    private static final String CLASS_KEY = classToConfig(Main.class);

    private static final Configuration.Element<Integer> MIN_SIZE_HEIGHT =
        new Configuration.Element<>(CLASS_KEY + "min_size.height", Integer::parseInt, null, 400);
    private static final Configuration.Element<Integer> MIN_SIZE_WIDTH =
        new Configuration.Element<>(CLASS_KEY + "min_size.width", Integer::parseInt, null, 600);

    private static final Configuration.Element<Integer> COORDINATE_X =
        new Configuration.Element<>(CLASS_KEY + "coordinate.x", Integer::parseInt, String::valueOf, 0);
    private static final Configuration.Element<Integer> COORDINATE_Y =
        new Configuration.Element<>(CLASS_KEY + "coordinate.y", Integer::parseInt, String::valueOf, 0);
    private static final Configuration.Element<Boolean> MAXIMIZED =
        new Configuration.Element<>(CLASS_KEY + "maximized", Boolean::parseBoolean, String::valueOf, false);
    private static final Configuration.Element<Integer> SIZE_HEIGHT =
        new Configuration.Element<>(CLASS_KEY + "size.height", Integer::parseInt, String::valueOf, 0);
    private static final Configuration.Element<Integer> SIZE_WIDTH =
        new Configuration.Element<>(CLASS_KEY + "size.width", Integer::parseInt, String::valueOf, 0);

    private final File file;

    private final ImagePanel imagePanel;

    public Main(File file, AnimationImage image) {
        this.file = file;

        FlatDarkLaf.setup();

        Configuration configuration = new Configuration(new File(WORK_DIR, "configuration.properties"));
        Configuration cache = new Configuration(new File(WORK_DIR, "cache.properties"));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new WindowsListener(cache));
        setMinimumSize(new Dimension(configuration.get(MIN_SIZE_WIDTH), configuration.get(MIN_SIZE_HEIGHT)));
        setLocation(new Point(cache.get(COORDINATE_X), cache.get(COORDINATE_Y)));
        if (cache.get(MAXIMIZED)) setExtendedState(JFrame.MAXIMIZED_BOTH);
        else setPreferredSize(new Dimension(cache.get(SIZE_WIDTH), cache.get(SIZE_HEIGHT)));
        setTitle(file.getName());

        add(imagePanel = new ImagePanel(configuration, image, this::openDirectory));
        pack();
        setVisible(true);
    }

    private void openDirectory() {
        try {
            Desktop.getDesktop().open(file.getParentFile());
        } catch (IOException e) {
            log.warn("Failed to open directory", e);
        }
    }

    public static synchronized void main(String[] args) {
        String message = work(args);
        if (message != null) System.out.println(message);
    }

    private static String work(String[] args) {
        if (args.length != 1) return "there must be exactly one argument";
        File file = new File(args[0]);
        if (!file.isFile()) return "argument must be file address";
        AnimationImage animationImage = new DecoderList(List.of(
            new DecoderGif(),
            new DecoderImage()
        )).decode(file);
        if (animationImage == null) return "file is not a image";

        SwingUtilities.invokeLater(() -> new Main(file, animationImage));
        return null;
    }

    @RequiredArgsConstructor
    private class WindowsListener extends WindowAdapter {
        final Configuration cache;

        @Override
        public void windowClosing(WindowEvent e) {
            imagePanel.stop();
            boolean maximized = JFrame.MAXIMIZED_BOTH == getExtendedState();
            cache.set(MAXIMIZED, maximized);
            Point location = getLocation();
            cache.set(COORDINATE_X, location.x);
            cache.set(COORDINATE_Y, location.y);
            if (!maximized) {
                Dimension size = getSize();
                cache.set(SIZE_HEIGHT, size.height);
                cache.set(SIZE_WIDTH, size.width);
            }

            cache.close();
        }
    }
}
