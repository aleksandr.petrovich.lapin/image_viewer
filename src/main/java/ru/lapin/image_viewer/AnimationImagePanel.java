package ru.lapin.image_viewer;

import lombok.Getter;
import ru.lapin.image_viewer.config.Configuration;
import ru.lapin.image_viewer.decoder.AnimationImage;

import javax.swing.Timer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.function.Consumer;

import static ru.lapin.image_viewer.config.Utils.clamp;
import static ru.lapin.image_viewer.config.Utils.classToConfig;

public class AnimationImagePanel extends JComponent {
    private static final String CLASS_KEY = classToConfig(AnimationImagePanel.class);

    private static final Configuration.Element<Boolean> SMOOTH_DECREASING =
        new Configuration.Element<>(CLASS_KEY + "smooth_decreasing", Boolean::parseBoolean, null, true);
    private static final Configuration.Element<Boolean> SMOOTH_INCREASING =
        new Configuration.Element<>(CLASS_KEY + "smooth_increasing", Boolean::parseBoolean, null, false);

    private static final int TIMER_DELTA = 10;

    private static final Map<Boolean, Object> SMOOTH = new HashMap<>() {{
        put(true, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        put(false, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
    }};

    private final BufferedImage[] frames;
    private final Configuration configuration;
    private final AnimationActionListener animationActionListener;
    private final Timer timer;
    private final int width;
    private final int height;

    private final Set<Consumer<Integer>> indexListeners;
    private final Set<Consumer<Boolean>> runListeners;
    private final Set<Consumer<Double>> timeListeners;

    private int i = 0;

    @Getter
    private double scale = 1;

    private int scaleWidth;

    @Getter
    private int scaleHeight;

    public AnimationImagePanel(Configuration configuration, AnimationImage animationImage) {
        this.configuration = configuration;

        this.frames = animationImage.getFrame();

        scaleWidth = width = animationImage.getWidth();
        scaleHeight = height = animationImage.getHeight();

        long[] delta = animationImage.getDelta();
        boolean isAnimated = delta != null && delta.length > 0;
        AnimationActionListener animationActionListener = null;
        Timer timer = null;
        if (isAnimated) {
            animationActionListener = new AnimationActionListener(delta);
            timer = new Timer(TIMER_DELTA, animationActionListener);
        }
        this.animationActionListener = animationActionListener;
        this.timer = timer;
        indexListeners = isAnimated ? new HashSet<>() : null;
        timeListeners = isAnimated ? new HashSet<>() : null;
        runListeners = isAnimated ? new HashSet<>() : null;
    }

    public void addIndexListener(Consumer<Integer> listener) {
        if (indexListeners != null) indexListeners.add(listener);
    }

    public void addRunListener(Consumer<Boolean> listener) {
        if (runListeners != null) runListeners.add(listener);
    }

    public void addTimeListener(Consumer<Double> listener) {
        if (timeListeners != null) timeListeners.add(listener);
    }

    public boolean isAnimated() {
        return timer != null;
    }

    public void start() {
        if (timer == null || timer.isRunning()) return;
        timer.start();
        forListener(runListeners, true);
    }

    public void stop() {
        if (timer == null || !timer.isRunning()) return;
        timer.stop();
        forListener(runListeners, false);
    }

    public void setTime(double time) {
        if (animationActionListener != null) animationActionListener.countIndex(time);
    }

    public void scale(double value) {
        scale = value;
        scaleWidth = (int) (width * value);
        scaleHeight = (int) (height * value);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public int getImageWidth() {
        return width;
    }

    public int getImageHeight() {
        return height;
    }

    public int countFrames() {
        return frames.length;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (g instanceof Graphics2D) ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION, scale > 1 ?
            SMOOTH.get(configuration.get(SMOOTH_INCREASING)) : SMOOTH.get(configuration.get(SMOOTH_DECREASING)));

        g.drawImage(frames[i], 0, 0, scaleWidth, scaleHeight, this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(scaleWidth, scaleHeight);
    }

    class AnimationActionListener implements ActionListener {
        final long[] times;
        final long lastTime;

        Long begin;

        AnimationActionListener(long[] delta) {
            times = new long[delta.length];
            long last = 0;
            for (int i = 0; i < delta.length; ++i) {
                last += delta[i];
                times[i] = last;
            }
            lastTime = last;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!isVisible()) return;
            long time = System.currentTimeMillis();
            if (begin == null) begin = time - TIMER_DELTA - (i > 0 ? times[i - 1] : 0);
            countIndex((time - begin) % lastTime);
        }

        void countIndex(double time) {
            countIndex((long) (clamp(time, 0, 1) * lastTime));
            begin = null;
        }

        void countIndex(long time) {
            forListener(timeListeners, (double) time / lastTime);
            int index = 0;
            while (times[index] < time) ++index;
            final int finalIndex = index;
            if (i == finalIndex) return;
            i = finalIndex;
            forListener(indexListeners, finalIndex);
            repaint();
        }
    }

    private static <T> void forListener(Collection<Consumer<T>> listeners, T value) {
        listeners.forEach(i -> i.accept(value));
    }
}
