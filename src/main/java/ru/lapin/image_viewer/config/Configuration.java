package ru.lapin.image_viewer.config;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Properties;
import java.util.function.Function;

@Slf4j
public class Configuration implements AutoCloseable {
    private final Properties properties = new Properties();

    private final File file;

    private boolean update = false;

    public Configuration(File file) {
        this.file = file;

        if (!file.isFile()) return;
        try (FileReader reader = new FileReader(file)) {
            properties.load(reader);
        } catch (Exception e) {
            log.warn("Could not open file at {}", file.getAbsoluteFile(), e);
        }
    }

    @Override
    public void close() {
        if (!update) return;
        File parentFile = file.getParentFile();
        if (!parentFile.isDirectory() && !parentFile.mkdirs()) {
            log.warn("Could not create parent directory {}", parentFile.getAbsolutePath());
            return;
        }
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            properties.store(fileOutputStream, null);
        } catch (Exception e) {
            log.warn("Could not write file at {}", file.getAbsolutePath(), e);
        }
    }

    public <R> R get(Element<R> element) {
        String stringValue = properties.getProperty(element.key);
        R value = null;
        if (stringValue != null) {
            try {
                value = element.convertor.apply(stringValue);
            } catch (Exception e) {
                log.warn("Could not convert {} (value: {})", element.key, stringValue, e);
            }
        }
        if (value == null) {
            log.debug("Configuration {} by address {} not found, default value will be used",
                element.key, file.getAbsoluteFile()
            );
            value = element.defaultValue;
        }
        return value;
    }

    public <R> void set(Element<R> element, R value) {
        if (value == null) properties.remove(element.key);
        else properties.setProperty(element.key, element.toString.apply(value));
        update = true;
    }

    @AllArgsConstructor
    public static class Element<R> {
        private final String key;
        private final Function<String, R> convertor;
        private final Function<R, String> toString;
        private final R defaultValue;
    }
}
