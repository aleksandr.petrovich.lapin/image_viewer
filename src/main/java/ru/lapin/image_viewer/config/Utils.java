package ru.lapin.image_viewer.config;

public class Utils {
    public static double clamp(double value, double min, double max) {
        return Math.min(Math.max(value, min), max);
    }

    public static String classToConfig(Class<?> typeClass) {
        StringBuilder builder = new StringBuilder();
        for (char ch : typeClass.getSimpleName().toCharArray()) {
            if (Character.isUpperCase(ch)) {
                if (!builder.isEmpty()) builder.append("_");
                builder.append(Character.toLowerCase(ch));
            } else builder.append(ch);
        }
        return builder.append(".").toString();
    }
}
