package ru.lapin.image_viewer.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static java.awt.image.BufferedImage.TYPE_INT_ARGB;

@Slf4j
@Getter
public class ImageSource {
    public static final ImageSource IS = new ImageSource();
    public static final int SIZE = 12;

    private static final String ONE_TO_ONE = "img/one_to_one.png";
    private static final String OPEN_DIRECTORY_ADDRESS = "img/open_directory.png";
    private static final String PLAY_ADDRESS = "img/play.png";
    private static final String STOP_ADDRESS = "img/stop.png";

    private final ImageIcon oneToOne;
    private final ImageIcon openDirectory;
    private final ImageIcon play;
    private final ImageIcon stop;

    public ImageSource() {
        oneToOne = loadIcon(ONE_TO_ONE);
        openDirectory = loadIcon(OPEN_DIRECTORY_ADDRESS);
        play = loadIcon(PLAY_ADDRESS);
        stop = loadIcon(STOP_ADDRESS);
    }

    private ImageIcon loadIcon(String address) {
        return Optional.ofNullable(loadImage(address)).map(ImageSource::resize).map(ImageIcon::new).orElse(null);
    }

    private BufferedImage loadImage(String address) {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(address)) {
            if (inputStream != null) return ImageIO.read(inputStream);
            log.error("Failed to load image by address {}", address);
        } catch (IOException e) {
            log.error("Failed to load image by address {}", address, e);
        }
        return null;
    }

    private static BufferedImage resize(BufferedImage image) {
        BufferedImage result = new BufferedImage(SIZE, SIZE, TYPE_INT_ARGB);
        Graphics2D graphics = result.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics.drawImage(image, 0, 0, SIZE, SIZE, 0, 0, image.getWidth(), image.getHeight(), null);
        graphics.dispose();
        return result;
    }
}
