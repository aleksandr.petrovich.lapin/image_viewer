package ru.lapin.image_viewer.decoder;

import lombok.extern.slf4j.Slf4j;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@Slf4j
public class DecoderGif implements Decoder {

    @Override
    public AnimationImage decode(File file) {
        GifDecoder image = new GifDecoder();
        try (InputStream is = new FileInputStream(file)) {
            if (image.read(is) != GifDecoder.STATUS_OK) return null;
        } catch (Exception e) {
            return null;
        }
        int count = image.getFrameCount();
        BufferedImage[] images = new BufferedImage[count];
        long[] delta = new long[count];
        for (int i = 0; i < count; ++i) {
            images[i] = image.getFrame(i);
            delta[i] = image.getDelay(i);
        }
        return new AnimationImage(delta, images);
    }
}
