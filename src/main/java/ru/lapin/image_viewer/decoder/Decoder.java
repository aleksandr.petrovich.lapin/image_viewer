package ru.lapin.image_viewer.decoder;

import java.io.File;

public interface Decoder {

    AnimationImage decode(File file);
}
