package ru.lapin.image_viewer.decoder;

import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@Slf4j
public class DecoderImage implements Decoder {

    @Override
    public AnimationImage decode(File file) {
        try (InputStream is = new FileInputStream(file)) {
            BufferedImage image = ImageIO.read(is);
            if (image != null) return new AnimationImage(null, new BufferedImage[]{image});
        } catch (Exception e) {
            log.error("could not create element decoder", e);
        }
        return null;
    }
}
