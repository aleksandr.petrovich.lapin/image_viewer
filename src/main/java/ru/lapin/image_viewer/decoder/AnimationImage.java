package ru.lapin.image_viewer.decoder;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.awt.image.BufferedImage;

@Getter
@RequiredArgsConstructor
public class AnimationImage {
    private final long[] delta;
    private final BufferedImage[] frame;

    public int getWidth() {
        return frame[0].getWidth();
    }

    public int getHeight() {
        return frame[0].getHeight();
    }
}
