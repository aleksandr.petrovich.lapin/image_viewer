package ru.lapin.image_viewer.decoder;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Collections;
import java.util.List;

@Slf4j
public class DecoderList implements Decoder {
    private final List<Decoder> decoders;

    public DecoderList(List<Decoder> decoders) {
        this.decoders = decoders == null ? Collections.emptyList() : decoders;
    }

    @Override
    public AnimationImage decode(File file) {
        for (Decoder decoder : decoders) {
            AnimationImage image = decoder.decode(file);
            if (image != null) return image;
        }
        return null;
    }
}
